import nltk
import sys
class Node:
	
	parent= None
	succ=[]
	result=None
	id=-1
	metode=""
	var_glob_in=dict()
	var_glob_out=dict()

	def __init__(self, id):
		self.id = id

	def setRelation(self, parent):

		
		if self.parent !=None:
			print "A child can just have a only one parent"
			return
		if parent !=None:
			if self.id == parent.id:
				print "Error"
			parent.succ.insert(len(parent.succ),self)			
			self.parent = parent
			
	

class Tree:
	
	num_nodes=0
	nodes=[]

	def createNode(self, metode, args, var_glob, nodePar):

		node = Node(self.num_nodes)
		node.succ=[]
		var_glob_in=dict()
		var_glob_out=dict()
		node.metode=metode
		node.args=args
		node.setRelation(nodePar)
		for i in var_glob:
			node.var_glob_in[var.keys()[var_glob.index(i)]]=i
	

		if node in self.nodes:
			print "Node already inserted"
		else:	
			self.nodes.append(node)
			self.num_nodes= self.num_nodes+1
		return node

	
	

	def finishNode1(self,node,  var_glob):
		for i in var_glob:
			node.var_glob_out[var.keys()[var_glob.index(i)]]=i
		

	def finishNode2(self,node,  var_glob, result):
			
		for i in var_glob:
			node.var_glob_out[var.keys()[var_glob.index(i)]]=i
		node.result=result

	def draw(self):

		for n in self.nodes:
			
			print"Node",`n.id`,"--------------"
			print"	metode:",n.metode
			print"	args:",n.args
			print"	result:",n.result
			if n.parent!=None:
				print"	parent:",n.parent.id
			
			childs=[i.id for i in n.succ]
			print"	childs:",childs, len(n.succ)
			print"	var_glob_in:", n.var_glob_in
			print"	var_glob_out:", n.var_glob_out
			print



def parse_args(line): return "["+line.split("(")[1].split(")")[0]+"]"

def parse_metode(line): return line.split(" ")[1].split("(")[0]

def parse_return(line, metodes): 
	ret=line.split("return")[1]
	return parse_call_function(ret, metodes)
		

def call_function(line, metodes):
	
	for i in metodes:
		
		if i+"(" in line and not("node" in line.split(i)[1]):
			return 1
	return 0

def parse_call_function(line, metodes):
	for i in metodes:
		if i in line :
			call = line.split(")")
			args=call[0].split("(")[1].split(",")
			
			if len(args)>1 or (len(args)==1 and args[0]!='') :
				line=line.replace(call[0], call[0]+",node")
			else :
				line=line.replace(call[0], call[0]+"node")
			return line
	return line	

 
def parse(prog,t):
	inside_func = 0
	var_glob=1
	prog_lines=prog.split("\n")
	i=0
	metodes=[]
	for line in prog_lines:
		
		if var_glob and "=" in line:
			var[line.split("=")[0]]= line.split("=")[1]
						
		
		if call_function(line, metodes):
			call=parse_call_function(line, metodes)
			prog_lines[prog_lines.index(line)]=call

		if "def " in line:
			var_glob=0
			inside_func=1
			args= parse_args(line)
			metode = parse_metode(line)
			metodes.append(metode)
			variabl="["+",".join(var.keys())+"]"
			header = line.split(")")
			
			if len(args)>2:
				line2=line.replace(header[0], header[0]+",nodePar")
			else :
				line2=line.replace(header[0], header[0]+"nodePar")
			prog_lines[prog_lines.index(line)]=line2
			
			
			prog_lines.insert(i+1,"	node=t.createNode("+metode+","+args+","+variabl+",nodePar)\n")
			
			
		elif inside_func==1 and "\t" not in line:
			inside_func = 0
			vars="["+",".join(var.keys())+"]"
			prog_lines.insert(i+1,"	t.finishNode1(node,"+vars+")\n")

		elif inside_func==1 and  "return" in line:
			inside_func = 0
			result=parse_return(line, metodes)
			vars="["+",".join(var.keys())+"]"
			prog_lines.insert(i,"	t.finishNode2(node,"+vars+","+result+")\n")
			line=line.replace(line.split("return")[1], result)
			line2=line.replace(line.split("return")[1]," node.result")
			prog_lines[prog_lines.index(line)]=line2
			
		
			
		i=i+1
		
	

	return "\n".join(prog_lines)

	

def load( name_file):

	f= open(name_file)
	prog = f.read()
	return prog
	
	
	
			
var=dict()	
node=None
t= Tree()

prog=load("prueba")
new_prog=parse(prog, t)
print new_prog
exec aux
t.draw()

